<?php

/**
 * @file
 * Hooks provided by the contact_storage_remote module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the fields to be stored.
 *
 * @param array $fields_to_store
 *   The fields with their values to store.
 * @param \Drupal\contact_storage_remote\RemoteStoragePluginInterface $plugin
 *   The remote storage plugin.
 * @param \Drupal\contact\MessageInterface $message
 *   The contact message being stored.
 */
function hook_contact_storage_remote_fields_to_store_alter(array &$fields_to_store, \Drupal\contact_storage_remote\RemoteStoragePluginInterface $plugin, \Drupal\contact\MessageInterface $message) {
  // Modify $mapped_fields_to_store here.
}

/**
 * Alter the list of predefined options to map a field to.
 *
 * @param array $field_mapping_options
 *   The predefined options.
 * @param array $context
 *   The context this is being called from. An associative array containing:
 *   - plugin: \Drupal\contact_storage_remote\RemoteStoragePluginInterface
 *   - contact_form: \Drupal\contact\ContactFormInterface
 *   - Field:  \Drupal\Core\Field\BaseFieldDefinition or
 *   \Drupal\Core\Field\Entity\BaseFieldOverride.
 */
function hook_contact_storage_remote_field_mapping_options_alter(array &$field_mapping_options, array $context) {
  // Modify $field_mapping_options here.
}

/**
 * @} End of "addtogroup hooks".
 */
