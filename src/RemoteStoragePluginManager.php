<?php

namespace Drupal\contact_storage_remote;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\contact\ContactFormInterface;
use Drupal\contact\MessageInterface;
use Drupal\contact_storage_remote\Annotation\ContactStorageRemoteStorage;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * RemoteStorage plugin manager.
 *
 * @package Drupal\contact_storage_remote
 */
class RemoteStoragePluginManager extends DefaultPluginManager {

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Condition storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $conditionStorage;

  /**
   * The plugins which have stored messages, by message id.
   *
   * @var string[]
   */
  protected $storedMessagePlugins = [];

  /**
   * RemoteStorage constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   The logger channel factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(\Traversable $namespaces, ModuleHandlerInterface $module_handler, LoggerChannelFactoryInterface $loggerChannelFactory, EntityTypeManagerInterface $entityTypeManager) {
    $this->subdir = 'Plugin/ContactStorageRemoteStorage';
    $this->namespaces = $namespaces;
    $this->pluginDefinitionAnnotationName = ContactStorageRemoteStorage::class;
    $this->pluginInterface = RemoteStoragePluginInterface::class;
    $this->moduleHandler = $module_handler;
    $this->logger = $loggerChannelFactory->get('contact_storage_remote');
    $this->conditionStorage = $entityTypeManager->getStorage('contact_storage_remote_condition');
  }

  /**
   * Get a list of enabled plugin_ids for the given contact_form.
   *
   * @param \Drupal\contact\ContactFormInterface $contact_form
   *   The contact form.
   *
   * @return string[]
   *   The list of plugin_ids enabled for this contact form.
   */
  public function getEnabledPlugins(ContactFormInterface $contact_form): array {
    $enabled_plugins = $contact_form->getThirdPartySetting('contact_storage_remote', 'enabled_plugin_ids');

    if (!is_array($enabled_plugins)) {
      $enabled_plugins = [];
    }

    return $enabled_plugins;
  }

  /**
   * Check if the given plugin is enabled for the given contact_form.
   *
   * @param \Drupal\contact\ContactFormInterface $contact_form
   *   The contact form.
   * @param string $plugin_id
   *   The plugin id.
   *
   * @return bool
   *   Enabled or not.
   */
  public function isPluginEnabled(ContactFormInterface $contact_form, string $plugin_id): bool {
    return in_array($plugin_id, $this->getEnabledPlugins($contact_form));
  }

  /**
   * Enable a plugin for the given contact form.
   *
   * @param \Drupal\contact\ContactFormInterface $contact_form
   *   The contact form.
   * @param string $plugin_id
   *   The plugin id.
   * @param bool $enabled
   *   Enabled or disabled.
   */
  public function setPluginEnabled(ContactFormInterface $contact_form, string $plugin_id, bool $enabled): void {
    $enabled_plugins = $this->getEnabledPlugins($contact_form);

    $index = array_search($plugin_id, $enabled_plugins);

    if ($enabled && $index === FALSE) {
      $enabled_plugins[] = $plugin_id;
    }

    if (!$enabled && $index !== FALSE) {
      unset($enabled_plugins[$index]);
    }

    $contact_form->setThirdPartySetting('contact_storage_remote', 'enabled_plugin_ids', $enabled_plugins);
    $contact_form->save();
  }

  /**
   * Get the field mapping for the given form/plugin.
   *
   * @param \Drupal\contact\ContactFormInterface $contact_form
   *   The contact form.
   * @param string $plugin_id
   *   The plugin.
   *
   * @return array
   *   The field mapping
   */
  public function getFieldMapping(ContactFormInterface $contact_form, string $plugin_id): array {
    $plugin_field_mappings = $contact_form->getThirdPartySetting('contact_storage_remote', 'plugin_field_mapping', []);

    foreach ($plugin_field_mappings as $plugin_field_mapping) {
      if ($plugin_field_mapping['plugin_id'] === $plugin_id) {
        return $plugin_field_mapping['mapping'];
      }
    }

    return [];
  }

  /**
   * Get a specific field mapping for a field.
   *
   * @param \Drupal\contact\ContactFormInterface $contact_form
   *   The contact form.
   * @param string $plugin_id
   *   The plugin.
   * @param string $field
   *   The field.
   *
   * @return string|null
   *   The field name which $field is mapped to or null if none is set.
   */
  public function getFieldMappingForField(ContactFormInterface $contact_form, string $plugin_id, string $field): ?string {
    $field_mappings = $this->getFieldMapping($contact_form, $plugin_id);

    foreach ($field_mappings as $field_mapping) {
      if ($field_mapping['field'] === $field) {
        return $field_mapping['maps_to'];
      }
    }

    return NULL;
  }

  /**
   * Set the field mapping for a plugin.
   *
   * @param \Drupal\contact\ContactFormInterface $contact_form
   *   The contact form.
   * @param string $plugin_id
   *   The plugin.
   * @param array $mapping
   *   The field mapping.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function setFieldMapping(ContactFormInterface $contact_form, string $plugin_id, array $mapping): void {
    $plugin_field_mappings = $contact_form->getThirdPartySetting('contact_storage_remote', 'plugin_field_mapping', []);
    $set = FALSE;
    foreach ($plugin_field_mappings as &$plugin_field_mapping) {
      if ($plugin_field_mapping['plugin_id'] === $plugin_id) {
        $plugin_field_mapping['mapping'] = $mapping;
        $set = TRUE;
        break;
      }
    }

    if (!$set) {
      $plugin_field_mappings[] = [
        'plugin_id' => $plugin_id,
        'mapping' => $mapping,
      ];
    }
    $contact_form->setThirdPartySetting('contact_storage_remote', 'plugin_field_mapping', $plugin_field_mappings);
    $contact_form->save();
  }

  /**
   * Check if a message matches all conditions set for the contact_form entity.
   *
   * @param \Drupal\contact\MessageInterface $message
   *   The message.
   *
   * @return bool
   *   True or false.
   */
  protected function matchesAllConditions(MessageInterface $message): bool {
    $contact_form = $message->getContactForm();

    $conditions = $this->conditionStorage->loadByProperties([
      'contactForm' => $contact_form->id(),
    ]);

    foreach ($conditions as $condition) {
      /**
       * @var \Drupal\contact_storage_remote\ConditionInterface $condition
       */
      $plugin = $condition->getPlugin();
      if (!$plugin->matches($message, $condition)) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Store the given message entity on the remote side.
   *
   * @param \Drupal\contact\MessageInterface $message
   *   The Message.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function store(MessageInterface $message): void {
    $contact_form = $message->getContactForm();

    if (!$this->matchesAllConditions($message)) {
      return;
    }

    $enabled_plugin_ids = $this->getEnabledPlugins($contact_form);

    foreach ($enabled_plugin_ids as $enabled_plugin_id) {
      try {
        /**
         * @var \Drupal\contact_storage_remote\RemoteStoragePluginInterface $plugin_instance
         */
        $plugin_instance = $this->createInstance($enabled_plugin_id);

        if (!$plugin_instance->allowEnabling()) {
          continue;
        }

        if (!$plugin_instance->store($message)) {
          throw new \RuntimeException('Could not store contact message ' . $message->id() . ' using plugin ' . $enabled_plugin_id);
        }
        else {
          $this->logger->info('Stored contact message ' . $message->id() . ' using plugin ' . $enabled_plugin_id);
        }

        if (!isset($this->storedMessagePlugins[$message->id()])) {
          $this->storedMessagePlugins[$message->id()] = [];
        }
        $this->storedMessagePlugins[$message->id()][] = $enabled_plugin_id;
      }
      catch (PluginNotFoundException $e) {
        // Plugin not found, skip it.
        continue;
      }
      catch (\Exception $e) {
        watchdog_exception('contact_storage_remote', $e);
      }
    }
  }

  /**
   * Get the plugin ids which have stored the given message.
   *
   * @param \Drupal\contact\MessageInterface $message
   *   The message.
   *
   * @return array
   *   The plugin ids.
   */
  public function getStoredMessagePlugins(MessageInterface $message): array {
    return $this->storedMessagePlugins[$message->id()] ?? [];
  }

}
