<?php

namespace Drupal\contact_storage_remote\Routing;

use Drupal\contact_storage_remote\Controller\RemoteStoragePluginFormController;
use Drupal\contact_storage_remote\RemoteStoragePluginManager;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Router that generates routes for plugin settings.
 */
class Router implements ContainerInjectionInterface {

  /**
   * The remote storage plugin manager.
   *
   * @var \Drupal\contact_storage_remote\RemoteStoragePluginManager
   */
  protected $remoteStoragePluginManager;

  /**
   * Constructor.
   *
   * @param \Drupal\contact_storage_remote\RemoteStoragePluginManager $remoteStoragePluginManager
   *   The remote storage plugin manager.
   */
  public function __construct(RemoteStoragePluginManager $remoteStoragePluginManager) {
    $this->remoteStoragePluginManager = $remoteStoragePluginManager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.contact_storage_remote.remote_storage')
    );
  }

  /**
   * Generate setting routes.
   *
   * @return \Symfony\Component\Routing\RouteCollection
   *   The routes.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function routes():RouteCollection {
    $routes = new RouteCollection();

    foreach ($this->remoteStoragePluginManager->getDefinitions() as $plugin_id => $definition) {
      /**
       * @var \Drupal\contact_storage_remote\RemoteStoragePluginInterface $instance
       */
      $instance = $this->remoteStoragePluginManager->createInstance($plugin_id);

      if (!$instance->allowEnabling()) {
        continue;
      }

      $routes->add(
        'contact_form.contact_storage_remote.storage.' . $plugin_id,
        new Route(
          '/admin/structure/contact/manage/{contact_form}/contact-storage-remote/' . $plugin_id,
          [
            '_controller' => RemoteStoragePluginFormController::class . '::form',
            '_title_callback' => RemoteStoragePluginFormController::class . '::title',
          ],
          [
            '_permission' => 'manage contact_storage_remote contact_form settings',
          ],
          [
            'parameters' => [
              'contact_form' => [
                'type' => 'entity:contact_form',
              ],
            ],
          ]
        )
      );
    }

    return $routes;
  }

}
