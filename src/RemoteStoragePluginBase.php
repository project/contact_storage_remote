<?php

namespace Drupal\contact_storage_remote;

use Drupal\contact\ContactFormInterface;
use Drupal\contact\MessageInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Abstract base class for a remote storage plugin.
 */
abstract class RemoteStoragePluginBase extends PluginBase implements RemoteStoragePluginInterface, ContainerFactoryPluginInterface {

  /**
   * Remote storage plugin manager.
   *
   * @var \Drupal\contact_storage_remote\RemoteStoragePluginManager
   */
  protected $remoteStoragePluginManager;

  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a \Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\contact_storage_remote\RemoteStoragePluginManager $remoteStoragePluginManager
   *   Remote storage plugin manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   Entity field manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entityDisplayRepository
   *   Entity display repository.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   The logger channel factory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RemoteStoragePluginManager $remoteStoragePluginManager, EntityFieldManagerInterface $entityFieldManager, EntityDisplayRepositoryInterface $entityDisplayRepository, ModuleHandlerInterface $moduleHandler, LoggerChannelFactoryInterface $loggerChannelFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->remoteStoragePluginManager = $remoteStoragePluginManager;
    $this->entityFieldManager = $entityFieldManager;
    $this->entityDisplayRepository = $entityDisplayRepository;
    $this->moduleHandler = $moduleHandler;
    $this->logger = $loggerChannelFactory->get('contact_storage_remote ' . $plugin_id);
  }

  /**
   * {@inheritDoc}
   */
  public function getId(): string {
    return $this->pluginDefinition['id'];
  }

  /**
   * {@inheritDoc}
   */
  public function getTitle(): string {
    return $this->pluginDefinition['title'];
  }

  /**
   * {@inheritDoc}
   */
  public function getDescription(): string {
    return $this->pluginDefinition['description'];
  }

  /**
   * Check if this plugin is enabled.
   *
   * @param \Drupal\contact\ContactFormInterface $contactForm
   *   Contact form.
   *
   * @return bool
   *   Enabled or not.
   */
  protected function isEnabled(ContactFormInterface $contactForm): bool {
    return $this->remoteStoragePluginManager->isPluginEnabled($contactForm, $this->getId());
  }

  /**
   * {@inheritDoc}
   */
  public function supportsFieldMapping(): bool {
    return $this->pluginDefinition['supports_field_mapping'];
  }

  /**
   * {@inheritDoc}
   */
  public function allowEnabling(): bool {
    return TRUE;
  }

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.contact_storage_remote.remote_storage'),
      $container->get('entity_field.manager'),
      $container->get('entity_display.repository'),
      $container->get('module_handler'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getSettingsForm(ContactFormInterface $contact_form, FormStateInterface $form_state): array {
    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function submitSettingsForm(ContactFormInterface $contact_form, FormStateInterface $form_state): void {
  }

  /**
   * Get the value for the specified field.
   *
   * This allows plugins to cast values to specific formats.
   *
   * @param \Drupal\contact\MessageInterface $message
   *   The message entity.
   * @param \Drupal\Core\Field\FieldDefinitionInterface|\Drupal\Core\Field\BaseFieldDefinition|\Drupal\Core\Field\Entity\BaseFieldOverride $field
   *   The field.
   * @param string $column
   *   The column of the field value to return.
   * @param string $maps_to_field_name
   *   The name the field value will be mapped to.
   *
   * @return mixed
   *   The field value
   */
  protected function getFieldValue(MessageInterface $message, $field, string $column, string $maps_to_field_name) {
    $values = $message->get($field->getName())->getValue();

    $field_value = [];
    foreach ($values as $value) {
      if (isset($value[$column]) && strlen($value[$column]) > 0) {
        $field_value[] = $value[$column] ?? NULL;
      }
    }

    return implode(',', $field_value);
  }

  /**
   * {@inheritDoc}
   */
  public function getFieldsToStore(MessageInterface $message): array {
    $fields_to_store = [];

    if ($this->supportsFieldMapping()) {
      $field_mappings = $this->remoteStoragePluginManager->getFieldMapping($message->getContactForm(), $this->getPluginId());

      foreach ($field_mappings as $field_mapping) {
        if (empty($field_mapping['field']) || empty($field_mapping['maps_to'])) {
          continue;
        }

        $field_mapping_field = explode('.', $field_mapping['field']);
        $field_mapping_field_column = array_pop($field_mapping_field);
        $field_mapping_field = implode('.', $field_mapping_field);

        if (!$message->hasField($field_mapping_field)) {
          continue;
        }

        $field_value = $this->getFieldValue($message, $message->get($field_mapping_field)
          ->getFieldDefinition(), $field_mapping_field_column, $field_mapping['maps_to']);

        if (empty($field_value)) {
          continue;
        }

        if (isset($fields_to_store[$field_mapping['maps_to']])) {
          if (!is_array($fields_to_store[$field_mapping['maps_to']])) {
            $fields_to_store[$field_mapping['maps_to']] = [
              $fields_to_store[$field_mapping['maps_to']],
              $field_value,
            ];
          }
          else {
            $fields_to_store[$field_mapping['maps_to']][] = $field_value;
          }
        }
        else {
          $fields_to_store[$field_mapping['maps_to']] = $field_value;
        }

      }
    }
    else {
      foreach ($message->getFields(TRUE) as $field) {
        $columns = array_keys($field->getFieldDefinition()
          ->getFieldStorageDefinition()
          ->getSchema()['columns']);
        foreach ($columns as $column) {
          $field_value = $this->getFieldValue($message, $field->getFieldDefinition(), $column, $field->getName());
          if (empty($field_value)) {
            continue;
          }
          $fields_to_store[$field->getName() . '.' . $column] = $field_value;
        }
      }
    }

    $this->moduleHandler->alter('contact_storage_remote_fields_to_store', $fields_to_store, $this, $message);

    return $fields_to_store;
  }

  /**
   * Get the predefined options fields can be mapped to.
   *
   * This should return a list of key=>value items which are used as predefined
   * mapping options. If NULL is returned, no predefined options are available,
   * and only a textfield will be shown where.
   *
   * @param \Drupal\contact\ContactFormInterface $contact_form
   *   The contact form.
   * @param \Drupal\Core\Field\BaseFieldDefinition|\Drupal\Core\Field\Entity\BaseFieldOverride $field
   *   The field we're mapping.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array|null
   *   The options or NULL if there are no predefined options to select
   */
  protected function getFieldMappingOptions(ContactFormInterface $contact_form, $field, FormStateInterface $form_state): ?array {
    return NULL;
  }

  /**
   * Get the field mapping form element ajax wrapper id.
   *
   * @return string
   *   The wrapper id.
   */
  protected function getFieldMappingFormElementAjaxWrapper(): string {
    return $this->getPluginId() . '_-field-mapping-wrapper';
  }

  /**
   * Ajax callback for refreshing the field mapping element.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The element.
   */
  public function refreshFieldMappingFormElementAjaxCallback(array $form, FormStateInterface $form_state): array {
    $form_state->setRebuild(TRUE);
    return $form[$this->getPluginId()]['fields'];
  }

  /**
   * {@inheritDoc}
   */
  public function buildFieldMappingFormElement(ContactFormInterface $contact_form, FormStateInterface $form_state): array {
    if (!$this->supportsFieldMapping()) {
      return [];
    }

    // Group fields by visible/invisible.
    $fields = $this->entityFieldManager->getFieldDefinitions('contact_message', $contact_form->id());
    $form_display = $this->entityDisplayRepository->getFormDisplay('contact_message', $contact_form->id());
    $form_display_fields = $form_display->getComponents();

    $field_groups = [
      'visible' => [],
      'invisible' => [],
    ];

    foreach ($fields as $field) {
      $field_name = $field->getName();
      if (isset($form_display_fields[$field_name])) {
        $field_groups['visible'][] = $field;
      }
      else {
        $field_groups['invisible'][] = $field;
      }
    }

    // Build the fields table.
    $table = [
      '#type' => 'table',
      '#header' => [
        $this->t('Field'),
        $this->t('Machine name'),
        $this->t('Is mapped to'),
        '',
      ],
    ];

    foreach ($field_groups as $fields) {
      foreach ($fields as $field) {
        $field_mapping_options = $this->getFieldMappingOptions($contact_form, $field, $form_state);
        $columns = $field->getFieldStorageDefinition()->getSchema()['columns'];
        $multi_column = count($columns) > 1;

        foreach (array_keys($columns) as $column) {
          $mapping = $this->remoteStoragePluginManager->getFieldMappingForField($contact_form, $this->getPluginId(), $field->getName() . '.' . $column);
          $maps_to_select_options = [];
          $maps_to_select_default_value = $mapping;

          if (is_array($field_mapping_options)) {
            if (count($field_mapping_options) === 0) {
              continue;
            }

            $maps_to_select_options = [
              '' => $this->t('None'),
            ];

            if ($this->pluginDefinition['supports_custom_field_mapping']) {
              $maps_to_select_options['custom'] = $this->t('Custom');
            }

            $maps_to_select_options += $field_mapping_options;
            $alter_context = [
              'plugin' => $this,
              'contact_form' => $contact_form,
              'field' => $field,
              'column' => $column,
            ];
            $this->moduleHandler->alter('contact_storage_remote_field_mapping_options', $maps_to_select_options, $alter_context);

            if ($mapping === NULL) {
              $maps_to_select_default_value = $field->getName();
              $maps_to_custom = $field->getName();
            }
            else {
              $maps_to_custom = $mapping;
              if (isset($maps_to_select_options[$mapping])) {
                $maps_to_select_default_value = $mapping;
                $maps_to_custom = '';
              }
              else {
                $maps_to_select_default_value = 'custom';
              }
            }
          }
          else {
            if ($mapping === NULL) {
              $maps_to_custom = $field->getName() . '.' . $column;
            }
            else {
              $maps_to_custom = $mapping;
            }
          }

          $table[$field->getName() . '.' . $column] = [
            'label' => [
              '#type' => 'markup',
              '#markup' => $field->getLabel() . ($multi_column ? ': ' . $column : ''),
            ],
            'name' => [
              '#type' => 'markup',
              '#markup' => $field->getName(),
            ],
          ];

          if ($field_mapping_options !== NULL) {
            $table[$field->getName() . '.' . $column]['maps_to_select'] = [
              '#type' => 'select',
              '#options' => $maps_to_select_options,
              '#default_value' => $maps_to_select_default_value,
              '#attributes' => [
                'style' => 'width:100%',
                'class' => [
                  'maps-to-select',
                ],
                'data-field-name' => $field->getName() . '.' . $column,
              ],
            ];
          }

          $table[$field->getName() . '.' . $column]['maps_to_custom'] = [
            '#type' => 'textfield',
            '#default_value' => $maps_to_custom,
            '#attributes' => [
              'data-field-name' => $field->getName(),
            ],
            '#states' => [
              'visible' => [
                ':input[name="field_mapping[' . $field->getName() . '.' . $column . '][maps_to_select]"]' => ['value' => 'custom'],
              ],
            ],
          ];

          if ($field_mapping_options === NULL) {
            $table[$field->getName() . '.' . $column]['maps_to_select'] = [
              '#type' => 'hidden',
              '#default_value' => 'custom',
            ];
          }
        }
      }
    }

    return [
      '#type' => 'fieldset',
      '#title' => $this->t('Form field mapping'),
      'field_mapping' => $table,
      '#attributes' => [
        'class' => [
          'contact-storage-remote-form-field-mapping',
        ],
      ],
      '#attached' => [
        'library' => [
          "contact_storage_remote/form-field-mapping",
        ],
      ],
      '#prefix' => '<div id="' . $this->getFieldMappingFormElementAjaxWrapper() . '">',
      '#suffix' => '</div>',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function submitFieldMappingFormElement(ContactFormInterface $contact_form, FormStateInterface $form_state): void {
    if (!$this->supportsFieldMapping()) {
      return;
    }

    $fields = $this->entityFieldManager->getFieldDefinitions('contact_message', $contact_form->id());

    $field_mapping = $form_state->getValue('field_mapping');
    $mapping = [];
    foreach ($fields as $field) {
      $columns = $field->getFieldStorageDefinition()->getSchema()['columns'];

      foreach (array_keys($columns) as $column) {
        if (!isset($field_mapping[$field->getName() . '.' . $column]) || empty($field_mapping[$field->getName() . '.' . $column])) {
          continue;
        }

        switch ($field_mapping[$field->getName() . '.' . $column]['maps_to_select']) {
          case '':
            $maps_to = '';
            break;

          case 'custom':
            $maps_to = $field_mapping[$field->getName() . '.' . $column]['maps_to_custom'];
            break;

          default:
            $maps_to = $field_mapping[$field->getName() . '.' . $column]['maps_to_select'];
        }

        $mapping[] = [
          'field' => $field->getName() . '.' . $column,
          'maps_to' => $maps_to,
        ];
      }
    }

    $this->remoteStoragePluginManager->setFieldMapping($contact_form, $this->getPluginId(), $mapping);
  }

  /**
   * {@inheritDoc}
   */
  public function validateSettingsForm(ContactFormInterface $contact_form, array $form, FormStateInterface $form_state): void {
  }

}
