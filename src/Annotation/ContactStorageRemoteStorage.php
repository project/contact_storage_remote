<?php

namespace Drupal\contact_storage_remote\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a ContactStorageRemoteStorage annotation object.
 *
 * @package Drupal\contact_storage_remote\Annotation
 *
 * @Annotation
 */
class ContactStorageRemoteStorage extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The plugin title.
   *
   * @var string
   */
  public $title;

  /**
   * The plugin description.
   *
   * @var string
   */
  public $description;

  /**
   * Does the plugin support mapping of fields.
   *
   * @var bool
   */
  public $supports_field_mapping = FALSE;

  /**
   * Custom field mapping name allowed.
   *
   * When $supports_field_mapping=true, is the user allowed to enter a
   * custom field name?
   *
   * @var bool
   */
  public $supports_custom_field_mapping = FALSE;

}
