<?php

namespace Drupal\contact_storage_remote\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a ContactStorageRemoteCondition annotation object.
 *
 * @package Drupal\contact_storage_remote\Annotation
 *
 * @Annotation
 */
class ContactStorageRemoteCondition extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The plugin title.
   *
   * @var string
   */
  public $title;

}
