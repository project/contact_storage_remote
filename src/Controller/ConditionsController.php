<?php

namespace Drupal\contact_storage_remote\Controller;

use Drupal\contact\ContactFormInterface;
use Drupal\contact_storage_remote\ConditionListBuilder;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Lists all conditions for a contact form.
 */
class ConditionsController extends ControllerBase {

  /**
   * The container.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
  protected $container;

  /**
   * The entity type.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $entityType;

  /**
   * Constructor.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(ContainerInterface $container) {
    $this->container = $container;

    /**
     * @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
     */
    $entity_type_manager = $container->get('entity_type.manager');
    $this->entityType = $entity_type_manager->getDefinition('contact_storage_remote_condition');
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container);
  }

  /**
   * Get the page title.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The title.
   */
  public function title() {
    return $this->t('Conditions');
  }

  /**
   * Get the list.
   *
   * @return array
   *   The render array.
   */
  public function build(Request $request) {
    $contact_form = $request->attributes->get('contact_form');

    if (!$contact_form instanceof ContactFormInterface) {
      throw new NotFoundHttpException();
    }
    $list_builder = ConditionListBuilder::createInstanceForContactForm($this->container, $this->entityType, $contact_form);
    return $list_builder->render();
  }

}
