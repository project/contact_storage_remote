<?php

namespace Drupal\contact_storage_remote\Controller;

use Drupal\contact\ContactFormInterface;
use Drupal\contact_storage_remote\RemoteStoragePluginManager;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Generates information about which plugins are enabled.
 */
class InfoController extends ControllerBase {

  /**
   * The remote storage plugin manager.
   *
   * @var \Drupal\contact_storage_remote\RemoteStoragePluginManager
   */
  protected $remoteStoragePluginManager;

  /**
   * Constructor.
   *
   * @param \Drupal\contact_storage_remote\RemoteStoragePluginManager $remoteStoragePluginManager
   *   The remote storage plugin manager.
   */
  public function __construct(RemoteStoragePluginManager $remoteStoragePluginManager) {
    $this->remoteStoragePluginManager = $remoteStoragePluginManager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.contact_storage_remote.remote_storage')
    );
  }

  /**
   * Build the info page.
   *
   * @param \Drupal\contact\ContactFormInterface|null $contact_form
   *   The contact form.
   *
   * @return array
   *   The render array
   */
  public function build(ContactFormInterface $contact_form = NULL) {
    $build = [];

    if (count($this->remoteStoragePluginManager->getDefinitions()) === 0) {
      return [
        '#markup' => $this->t('No contact storage remote plugins available.'),
      ];
    }

    foreach ($this->remoteStoragePluginManager->getDefinitions() as $plugin_id => $definition) {
      $build[$plugin_id] = [
        '#type' => 'fieldset',
        '#title' => $definition['title'],
        'enabled' => [
          '#markup' => $this->remoteStoragePluginManager->isPluginEnabled($contact_form, $plugin_id) ? $this->t('Enabled') : $this->t('Not enabled'),
        ],
      ];
    }

    return $build;
  }

}
