<?php

namespace Drupal\contact_storage_remote\Controller;

use Drupal\contact\ContactFormInterface;
use Drupal\contact_storage_remote\Form\RemoteStorageSettingsForm;
use Drupal\contact_storage_remote\RemoteStoragePluginManager;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller for plugin settings routes.
 */
class RemoteStoragePluginFormController extends ControllerBase {

  /**
   * The plugin instance.
   *
   * @var \Drupal\contact_storage_remote\RemoteStoragePluginInterface
   */
  protected $pluginInstance;

  /**
   * The remote storage plugin manager.
   *
   * @var \Drupal\contact_storage_remote\RemoteStoragePluginManager
   */
  protected $remoteStoragePluginManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The current route match.
   * @param \Drupal\contact_storage_remote\RemoteStoragePluginManager $remoteStoragePluginManager
   *   The remote storage plugin manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(RouteMatchInterface $routeMatch, RemoteStoragePluginManager $remoteStoragePluginManager) {
    preg_match('/contact_form\.contact_storage_remote\.storage\.(.*)/', $routeMatch->getRouteName(), $matches);

    if (count($matches) !== 2 || empty($matches[1])) {
      throw new \RuntimeException('Invalid route name: ' . $routeMatch->getRouteName());
    }

    $this->pluginInstance = $remoteStoragePluginManager->createInstance($matches[1]);
    $this->remoteStoragePluginManager = $remoteStoragePluginManager;

    if (!$this->pluginInstance->allowEnabling()) {
      throw new \RuntimeException('Plugin not allowed');
    }
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('plugin.manager.contact_storage_remote.remote_storage')
    );
  }

  /**
   * Get the title.
   *
   * @param \Drupal\contact\ContactFormInterface|null $contact_form
   *   The contact forn.
   *
   * @return string
   *   The title
   */
  public function title(ContactFormInterface $contact_form = NULL) {
    return $this->pluginInstance->getTitle();
  }

  /**
   * Build the form.
   *
   * @param \Drupal\contact\ContactFormInterface|null $contact_form
   *   The contact form.
   *
   * @return array
   *   The render array
   */
  public function form(ContactFormInterface $contact_form = NULL) {

    if (!$contact_form instanceof ContactFormInterface) {
      throw new NotFoundHttpException();
    }

    $form = new RemoteStorageSettingsForm($this->remoteStoragePluginManager, $this->pluginInstance, $contact_form);

    return $this->formBuilder()->getForm($form);
  }

}
