<?php

namespace Drupal\contact_storage_remote\Plugin\ContactStorageRemoteCondition;

use Drupal\contact\Entity\Message;
use Drupal\contact\MessageInterface;
use Drupal\contact_storage_remote\ConditionInterface;
use Drupal\contact_storage_remote\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin checks for a field value condition.
 *
 * @ContactStorageRemoteCondition(
 *   id = "fieldvalue",
 *   title = @Translation("Field value")
 * )
 */
class FieldValue extends ConditionPluginBase {

  /**
   * {@inheritDoc}
   */
  public function getSettingsForm(ConditionInterface $condition, FormStateInterface $form_state): array {

    $message = Message::create([
      'contact_form' => $condition->getContactForm()->id(),
    ]);

    $user_input = $form_state->getUserInput();
    $fieldvalue_field = isset($user_input['fieldvalue_field']) && !empty($user_input['fieldvalue_field']) ? $user_input['fieldvalue_field'] : $condition->getSetting('field');

    $form['fieldvalue_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Field'),
      '#options' => [],
      '#required' => TRUE,
      '#default_value' => $fieldvalue_field,
      '#ajax' => [
        'callback' => __CLASS__ . '::ajaxSelectFieldValueFieldCallback',
        'wrapper' => 'fieldvalue-property',
      ],
    ];

    foreach ($message->getFields() as $field_name => $field) {
      $form['fieldvalue_field']['#options'][$field_name] = $field->getFieldDefinition()
        ->getLabel();
    }

    $fieldvalue_property_options = [];
    $property = $condition->getSetting('property');
    if (!empty($fieldvalue_field) && $message->hasField($fieldvalue_field)) {
      $schema = $message->get($fieldvalue_field)
        ->getFieldDefinition()
        ->getFieldStorageDefinition()
        ->getSchema();

      if (!empty($property) && !isset($schema['columns'][$property])) {
        $property = NULL;
      }

      foreach ($schema['columns'] as $column => $data) {
        if (empty($property)) {
          $property = $column;
        }
        $fieldvalue_property_options[$column] = $column;
      }
    }

    $form['fieldvalue_property'] = [
      '#type' => 'select',
      '#title' => $this->t('Property'),
      '#options' => $fieldvalue_property_options,
      '#required' => TRUE,
      '#default_value' => $property,
      '#prefix' => '<div id="fieldvalue-property">',
      '#suffix' => '</div>',
    ];

    $form['fieldvalue_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value'),
      '#default_value' => $condition->getSetting('value'),
    ];

    return $form;
  }

  /**
   * Re-render the property field.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed
   *   The render array
   */
  public static function ajaxSelectFieldValueFieldCallback(array &$form, FormStateInterface $form_state) {
    return $form['condition_plugin']['plugin_settings_form']['fieldvalue_property'];
  }

  /**
   * {@inheritDoc}
   */
  public function submitSettingsForm(ConditionInterface $condition, FormStateInterface $form_state): void {
    $condition->setSetting('field', $form_state->getValue('fieldvalue_field'));
    $condition->setSetting('property', $form_state->getValue('fieldvalue_property'));
    $condition->setSetting('value', $form_state->getValue('fieldvalue_value'));
  }

  /**
   * {@inheritDoc}
   */
  public function matches(MessageInterface $message, ConditionInterface $condition): bool {
    $field = $condition->getSetting('field');
    $property = $condition->getSetting('property');
    $value = $condition->getSetting('value');

    if (empty($field) || !$message->hasField($field)) {
      return FALSE;
    }

    $field_value = $message->get($field)->getValue();

    return isset($field_value[0][$property]) && $field_value[0][$property] == $value;
  }

  /**
   * {@inheritDoc}
   */
  public function getSummary(ConditionInterface $condition): string {
    $field = $condition->getSetting('field');
    $property = $condition->getSetting('property');
    $value = $condition->getSetting('value');

    return $field . '.' . $property . ' = ' . $value;
  }

}
