<?php

namespace Drupal\contact_storage_remote;

use Drupal\contact\ContactFormInterface;
use Drupal\contact\MessageInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Interface for a remote storage plugin.
 */
interface RemoteStoragePluginInterface {

  /**
   * Get the plugin id.
   *
   * @return string
   *   The plugin id.
   */
  public function getId(): string;

  /**
   * Get the plugin title.
   *
   * @return string
   *   The title.
   */
  public function getTitle(): string;

  /**
   * Get the plugin description.
   *
   * @return string
   *   The description.
   */
  public function getDescription(): string;

  /**
   * Check if the plugin supports field mapping.
   *
   * @return bool
   *   True or false.
   */
  public function supportsFieldMapping(): bool;

  /**
   * Can this plugin be enabled?
   *
   * @return bool
   *   Yes or no.
   */
  public function allowEnabling(): bool;

  /**
   * Get the settings form for this plugin.
   *
   * This form will be appended to the
   * \Drupal\contact_storage_remote\Form\RemoteStorageSettingsForm form.
   *
   * @param \Drupal\contact\ContactFormInterface $contact_form
   *   The contact form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form elements.
   */
  public function getSettingsForm(ContactFormInterface $contact_form, FormStateInterface $form_state): array;

  /**
   * Validate the settings form.
   *
   * @param \Drupal\contact\ContactFormInterface $contact_form
   *   The contact form.
   * @param array $form
   *   The settings form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateSettingsForm(ContactFormInterface $contact_form, array $form, FormStateInterface $form_state): void;

  /**
   * Process the submitted settings form values.
   *
   * @param \Drupal\contact\ContactFormInterface $contact_form
   *   The contact form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function submitSettingsForm(ContactFormInterface $contact_form, FormStateInterface $form_state): void;

  /**
   * Build the field mapping form element.
   *
   * @param \Drupal\contact\ContactFormInterface $contact_form
   *   The contact form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The element.
   */
  public function buildFieldMappingFormElement(ContactFormInterface $contact_form, FormStateInterface $form_state): array;

  /**
   * Submit/save the field mapping.
   *
   * @param \Drupal\contact\ContactFormInterface $contact_form
   *   The contact form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function submitFieldMappingFormElement(ContactFormInterface $contact_form, FormStateInterface $form_state): void;

  /**
   * Get the (mapped) array of fields to store.
   *
   * @param \Drupal\contact\MessageInterface $message
   *   The message.
   *
   * @return array
   *   The array of fields.
   */
  public function getFieldsToStore(MessageInterface $message): array;

  /**
   * Store the contact message on the remote side.
   *
   * @param \Drupal\contact\MessageInterface $message
   *   The contact message entity.
   *
   * @return bool
   *   TRUE on success, FALSE on failure.
   */
  public function store(MessageInterface $message): bool;

}
