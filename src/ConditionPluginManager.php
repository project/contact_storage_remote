<?php

namespace Drupal\contact_storage_remote;

use Drupal\contact_storage_remote\Annotation\ContactStorageRemoteCondition;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Condition plugin manager.
 *
 * @package Drupal\contact_storage_remote
 */
class ConditionPluginManager extends DefaultPluginManager {

  /**
   * Constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, ModuleHandlerInterface $module_handler) {
    $this->subdir = 'Plugin/ContactStorageRemoteCondition';
    $this->namespaces = $namespaces;
    $this->pluginDefinitionAnnotationName = ContactStorageRemoteCondition::class;
    $this->pluginInterface = ConditionPluginInterface::class;
    $this->moduleHandler = $module_handler;
  }

}
