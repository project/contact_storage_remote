<?php

namespace Drupal\contact_storage_remote\Form;

use Drupal\contact\Entity\ContactForm;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Manage mail settings for a contact_form.
 */
class RemoteStorageMailSettingsForm extends FormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'contact_storage_remote_storage_mail_settings_form';
  }

  /**
   * Get the form page title.
   *
   * @param \Drupal\contact\Entity\ContactForm|null $contact_form
   *   The contact form.
   *
   * @return string
   *   The title.
   */
  public function title(ContactForm $contact_form = NULL): string {
    return $this->t('Mail settings for @contact_form', ['@contact_form' => $contact_form !== NULL ? $contact_form->label() : $this->t('contact form')]);
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ContactForm $contact_form = NULL) {

    if ($contact_form === NULL) {
      throw new NotFoundHttpException();
    }

    $form['#contact_form'] = $contact_form;

    $form['prevent_mail_when_stored'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Prevent mails on succesfull storage'),
      '#description' => $this->t('Enable this to prevent the contact module from sending e-mails when the message is succesfully stored in at least one remote storage.'),
      '#default_value' => $contact_form->getThirdPartySetting('contact_storage_remote', 'prevent_mail_when_stored'),
    ];

    $form['actions']['#type'] = 'actions';

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form['#contact_form']->setThirdPartySetting('contact_storage_remote', 'prevent_mail_when_stored', $form_state->getValue('prevent_mail_when_stored'));
    $form['#contact_form']->save();
    $this->messenger()->addStatus($this->t('Your settings have been saved.'));
  }

}
