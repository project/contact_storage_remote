<?php

namespace Drupal\contact_storage_remote\Form;

use Drupal\Core\Entity\EntityDeleteForm;
use Drupal\Core\Url;

/**
 * Delete a condition.
 */
class ConditionDeleteForm extends EntityDeleteForm {

  /**
   * {@inheritDoc}
   */
  public function getCancelUrl() {
    /**
     * @var \Drupal\contact_storage_remote\ConditionInterface $condition
     */
    $condition = $this->entity;

    return Url::fromRoute('entity.contact_form.contact_storage_remote.conditions', [
      'contact_form' => $condition->getContactForm()
        ->id(),
    ]);
  }

}
