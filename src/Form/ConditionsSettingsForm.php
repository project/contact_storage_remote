<?php

namespace Drupal\contact_storage_remote\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Manage remote storage settings for a contact_form.
 */
class ConditionsSettingsForm extends FormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'contact_storage_remote_storage_conditions_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['actions']['#type'] = 'actions';

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
