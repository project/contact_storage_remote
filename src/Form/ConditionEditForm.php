<?php

namespace Drupal\contact_storage_remote\Form;

use Drupal\contact\ContactFormInterface;
use Drupal\contact_storage_remote\ConditionPluginManager;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Edit a condition.
 */
class ConditionEditForm extends EntityForm {

  /**
   * The condition plugin manager.
   *
   * @var \Drupal\contact_storage_remote\ConditionPluginManager
   */
  protected $conditionPluginManager;

  /**
   * Constructor.
   *
   * @param \Drupal\contact_storage_remote\ConditionPluginManager $conditionPluginManager
   *   The condition plugin manager.
   */
  public function __construct(ConditionPluginManager $conditionPluginManager) {
    $this->conditionPluginManager = $conditionPluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.contact_storage_remote.condition'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['#contact_form'] = $this->getRequest()->attributes->get('contact_form');

    if (!$form['#contact_form'] instanceof ContactFormInterface) {
      throw new \InvalidArgumentException('Contact form missing');
    }

    /**
     * @var \Drupal\contact_storage_remote\ConditionInterface $condition
     */
    $condition = $this->entity;

    if (!$condition->isNew() && $condition->getContactForm()
      ->id() !== $form['#contact_form']->id()) {
      throw new \InvalidArgumentException('Contact form mismatch');
    }
    if ($condition->isNew()) {
      $condition->setContactForm($form['#contact_form']);
    }
    $user_input = $form_state->getUserInput();
    $plugin_id = isset($user_input['plugin_id']) && !empty($user_input['plugin_id']) ? $user_input['plugin_id'] : $condition->getPluginId();

    $form['plugin_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Plugin'),
      '#default_value' => $plugin_id,
      '#required' => TRUE,
      '#options' => [],
      '#ajax' => [
        'callback' => '::ajaxSelectPluginCallback',
        'wrapper' => 'condition-plugin',
      ],
    ];

    foreach ($this->conditionPluginManager->getDefinitions() as $defined_plugin_id => $definition) {
      $form['plugin_id']['#options'][$defined_plugin_id] = $definition['title'];
    }

    $form['condition_plugin'] = [
      '#type' => 'container',
      '#prefix' => '<div id="condition-plugin">',
      '#suffix' => '</div>',
    ];

    if (!empty($plugin_id)) {
      /**
       * @var \Drupal\contact_storage_remote\ConditionPluginInterface $plugin
       */
      $plugin = $this->conditionPluginManager->createInstance($plugin_id);

      $form['condition_plugin']['plugin_settings_form'] = $plugin->getSettingsForm($condition, $form_state);
    }
    elseif (!empty($plugin_id)) {
      $form['condition_plugin']['error'] = [
        '#type' => 'markup',
        '#markup' => 'Error: ' . $plugin_id,
      ];
    }

    return $form;
  }

  /**
   * Re-render the condition plugin.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed
   *   The render array
   */
  public function ajaxSelectPluginCallback(array &$form, FormStateInterface $form_state) {
    return $form['condition_plugin'];
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /**
     * @var \Drupal\contact_storage_remote\ConditionInterface $condition
     */
    $condition = $this->entity;

    $plugin_id = $form_state->getValue('plugin_id');
    /**
     * @var \Drupal\contact_storage_remote\ConditionPluginInterface $plugin
     */
    $plugin = $this->conditionPluginManager->createInstance($plugin_id);

    $plugin->submitSettingsForm($condition, $form_state);
    $condition->setPluginId($plugin_id);
    $condition->setContactForm($form['#contact_form']);

    $status = $condition->save();

    if ($status == SAVED_UPDATED) {
      $this->messenger()
        ->addStatus($this->t('Condition %label has been updated.', ['%label' => $condition->label()]));
      $this->logger('contact_storage_remote')
        ->notice('Condition %label has been updated.', ['%label' => $condition->label()]);
    }
    else {
      $this->messenger()
        ->addStatus($this->t('Condition %label has been added.', ['%label' => $condition->label()]));
      $this->logger('contact_storage_remote')
        ->notice('Condition %label has been added.', ['%label' => $condition->label()]);
    }

    return $status;
  }

}
