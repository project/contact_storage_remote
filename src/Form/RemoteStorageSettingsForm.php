<?php

namespace Drupal\contact_storage_remote\Form;

use Drupal\contact\ContactFormInterface;
use Drupal\contact_storage_remote\RemoteStoragePluginInterface;
use Drupal\contact_storage_remote\RemoteStoragePluginManager;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Manage remote storage settings for a contact_form.
 */
class RemoteStorageSettingsForm extends FormBase {

  /**
   * The remote storage plugin manager.
   *
   * @var \Drupal\contact_storage_remote\RemoteStoragePluginManager
   */
  protected $remoteStoragePluginManager;

  /**
   * The plugin instance.
   *
   * @var \Drupal\contact_storage_remote\RemoteStoragePluginInterface
   */
  protected $pluginInstance;

  /**
   * The contact form.
   *
   * @var \Drupal\contact\ContactFormInterface
   */
  protected $contactForm;

  /**
   * Constructor.
   *
   * @param \Drupal\contact_storage_remote\RemoteStoragePluginManager $remoteStoragePluginManager
   *   The remote storage plugin manager.
   * @param \Drupal\contact_storage_remote\RemoteStoragePluginInterface $pluginInstance
   *   The plugin instance.
   * @param \Drupal\contact\ContactFormInterface $contactForm
   *   The contact form.
   */
  public function __construct(RemoteStoragePluginManager $remoteStoragePluginManager, RemoteStoragePluginInterface $pluginInstance, ContactFormInterface $contactForm) {
    $this->remoteStoragePluginManager = $remoteStoragePluginManager;
    $this->pluginInstance = $pluginInstance;
    $this->contactForm = $contactForm;
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'contact_storage_remote_storage_settings_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#contact_form'] = $this->contactForm;

    $enabled_plugin_ids = $this->remoteStoragePluginManager->getEnabledPlugins($this->contactForm);

    $plugin_definitions = $this->remoteStoragePluginManager->getDefinitions();
    if (count($plugin_definitions) === 0) {
      $form['no_plugins'] = [
        '#type' => 'markup',
        '#markup' => $this->t('No contact storage remote plugins found.'),
      ];
    }
    else {
      if ($this->pluginInstance->allowEnabling()) {

        $enabled = in_array($this->pluginInstance->getId(), $enabled_plugin_ids);
        $form['description'] = [
          '#markup' => $this->pluginInstance->getDescription(),
        ];
        $form[$this->pluginInstance->getId()] = [
          '#type' => 'container',
          $this->pluginInstance->getId() . '_enabled' => [
            '#type' => 'checkbox',
            '#title' => $this->t('Enabled'),
            '#default_value' => $enabled,
          ],
        ];

        if (!empty($plugin_settings_form = $this->pluginInstance->getSettingsForm($this->contactForm, $form_state))) {
          $form[$this->pluginInstance->getId()][$this->pluginInstance->getId() . '_settings'] = $plugin_settings_form;
        }

        if ($this->pluginInstance->supportsFieldMapping()) {
          $form[$this->pluginInstance->getId()]['fields'] = $this->pluginInstance->buildFieldMappingFormElement($this->contactForm, $form_state);
        }

        $form['actions']['#type'] = 'actions';

        $form['actions']['submit'] = [
          '#type' => 'submit',
          '#value' => $this->t('Save'),
          '#button_type' => 'primary',
        ];
      }
    }

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $this->pluginInstance->validateSettingsForm($form['#contact_form'], $form[$this->pluginInstance->getId()][$this->pluginInstance->getId() . '_settings'], $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    if (!$this->pluginInstance->allowEnabling()) {
      $this->remoteStoragePluginManager->setPluginEnabled($form['#contact_form'], $this->pluginInstance->getId(), FALSE);
    }
    else {

      $this->remoteStoragePluginManager->setPluginEnabled($form['#contact_form'], $this->pluginInstance->getId(), $form_state->getValue($this->pluginInstance->getId() . '_enabled'));

      $this->pluginInstance->submitSettingsForm($form['#contact_form'], $form_state);
      if ($this->pluginInstance->supportsFieldMapping()) {
        $this->pluginInstance->submitFieldMappingFormElement($form['#contact_form'], $form_state);
      }
    }
    $this->messenger()->addStatus($this->t('Your settings have been saved.'));
  }

}
