<?php

namespace Drupal\contact_storage_remote\Entity;

use Drupal\contact\ContactFormInterface;
use Drupal\contact_storage_remote\ConditionInterface;
use Drupal\contact_storage_remote\ConditionPluginInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the condition entity.
 *
 * @ConfigEntityType(
 *   id = "contact_storage_remote_condition",
 *   label = @Translation("Condition"),
 *   label_collection = @Translation("Conditions"),
 *   label_singular = @Translation("condition"),
 *   label_plural = @Translation("conditions"),
 *   label_count = @PluralTranslation(
 *     singular = "@count condition",
 *     plural = "@count conditions",
 *   ),
 *   handlers = {
 *     "access" =
 *   "Drupal\contact_storage_remote\ConditionAccessControlHandler",
 *     "list_builder" = "Drupal\contact_storage_remote\ConditionListBuilder",
 *     "form" = {
 *       "add" = "Drupal\contact_storage_remote\Form\ConditionEditForm",
 *       "edit" = "Drupal\contact_storage_remote\Form\ConditionEditForm",
 *       "delete" = "Drupal\contact_storage_remote\Form\ConditionDeleteForm"
 *     }
 *   },
 *   config_prefix = "contact_storage_remote_condition",
 *   admin_permission = "manage contact_storage_remote contact_form settings",
 *   entity_keys = {
 *     "id" = "id"
 *   },
 *   links = {
 *     "delete-form" =
 *   "/admin/structure/contact/manage/{contact_form}/contact-storage-remote/conditions/{contact_storage_remote_condition}/delete",
 *     "edit-form" =
 *   "/admin/structure/contact/manage/{contact_form}/contact-storage-remote/conditions/{contact_storage_remote_condition}/edit",
 *     "collection" =
 *   "/admin/structure/contact/manage/{contact_form}/contact-storage-remote/conditions"
 *   },
 *   config_export = {
 *     "id",
 *     "contactForm",
 *     "plugin",
 *     "settings",
 *   }
 * )
 */
class Condition extends ConfigEntityBase implements ConditionInterface {

  /**
   * {@inheritDoc}
   */
  protected function urlRouteParameters($rel) {
    return [
      'contact_form' => $this->getContactForm() !== NULL ? $this->getContactForm()
        ->id() : 'none',
      'contact_storage_remote_condition' => $this->id(),
    ];
  }

  /**
   * The condition ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The contact form.
   *
   * @var string
   */
  protected $contactForm;

  /**
   * The condition plugin id.
   *
   * @var string
   */
  protected $plugin;

  /**
   * The condition settings.
   *
   * @var mixed[]
   */
  protected $settings = [];

  /**
   * {@inheritDoc}
   */
  public function label() {
    $plugin = $this->getPlugin();
    if ($plugin === NULL) {
      return $this->id();
    }

    return $plugin->getSummary($this);
  }

  /**
   * {@inheritDoc}
   */
  public function getContactForm(): ?ContactFormInterface {
    if (empty($this->contactForm)) {
      return NULL;
    }
    return \Drupal::entityTypeManager()
      ->getStorage('contact_form')
      ->load($this->contactForm);
  }

  /**
   * {@inheritDoc}
   */
  public function setContactForm(ContactFormInterface $contactForm): void {
    $this->contactForm = $contactForm->id();
  }

  /**
   * {@inheritDoc}
   */
  public function getPluginId(): ?string {
    return $this->plugin;
  }

  /**
   * {@inheritDoc}
   */
  public function getPlugin(): ?ConditionPluginInterface {
    /**
     * @var \Drupal\contact_storage_remote\ConditionPluginManager $plugin_manager
     */
    $plugin_manager = \Drupal::service('plugin.manager.contact_storage_remote.condition');

    $plugin_id = $this->getPluginId();

    if (!empty($plugin_id)) {
      return $plugin_manager->createInstance($plugin_id);
    }
    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function setPluginId(string $pluginId): void {
    $this->plugin = $pluginId;
  }

  /**
   * {@inheritDoc}
   */
  public function getSetting(string $setting) {
    return $this->settings[$setting] ?? NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function setSetting(string $setting, $value): void {
    $this->settings[$setting] = $value;
  }

  /**
   * {@inheritDoc}
   */
  public function postCreate(EntityStorageInterface $storage) {
    if (empty($this->id)) {
      $this->id = \Drupal::service('uuid')->generate();
    }
    parent::postCreate($storage);
  }

}
