<?php

namespace Drupal\contact_storage_remote;

use Drupal\contact\ContactFormInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface for a condition.
 */
interface ConditionInterface extends ConfigEntityInterface {

  /**
   * Get the contact_form.
   *
   * @return \Drupal\contact\ContactFormInterface|null
   *   The contact_form.
   */
  public function getContactForm(): ?ContactFormInterface;

  /**
   * Set the contact_form.
   *
   * @param \Drupal\contact\ContactFormInterface $contactForm
   *   The contact_form.
   */
  public function setContactForm(ContactFormInterface $contactForm): void;

  /**
   * Get the plugin id.
   *
   * @return string|null
   *   The plugin id.
   */
  public function getPluginId(): ?string;

  /**
   * Set the plugin id.
   *
   * @param string $pluginId
   *   The plugin id.
   */
  public function setPluginId(string $pluginId): void;

  /**
   * Get the plugin instance.
   *
   * @return \Drupal\contact_storage_remote\ConditionPluginInterface|null
   *   The instance.
   */
  public function getPlugin(): ?ConditionPluginInterface;

  /**
   * Get a setting.
   *
   * @param string $setting
   *   The setting.
   *
   * @return mixed
   *   The value
   */
  public function getSetting(string $setting);

  /**
   * Set setting.
   *
   * @param string $setting
   *   The setting.
   * @param mixed $value
   *   The value.
   */
  public function setSetting(string $setting, $value): void;

}
