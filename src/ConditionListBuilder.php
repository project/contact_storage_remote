<?php

namespace Drupal\contact_storage_remote;

use Drupal\contact\ContactFormInterface;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The list builder.
 */
class ConditionListBuilder extends ConfigEntityListBuilder {

  /**
   * The contact form.
   *
   * @var \Drupal\contact\ContactFormInterface
   */
  protected $contactForm;

  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\contact\ContactFormInterface $contactForm
   *   The contact form.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, ContactFormInterface $contactForm) {
    parent::__construct($entity_type, $storage);
    $this->contactForm = $contactForm;
  }

  /**
   * Instantiates a new instance of this entity handler.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container this object should use.
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\contact\ContactFormInterface $contactForm
   *   The contact form.
   *
   * @return static
   *   A new instance of the entity handler.
   */
  public static function createInstanceForContactForm(ContainerInterface $container, EntityTypeInterface $entity_type, ContactFormInterface $contactForm) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $contactForm
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Condition');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['id'] = $entity->label();

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->condition('contactForm', $this->contactForm->id())
      ->sort($this->entityType->getKey('id'));

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

}
