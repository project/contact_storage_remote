<?php

namespace Drupal\contact_storage_remote;

use Drupal\contact\MessageInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Interface for a remote storage condition.
 */
interface ConditionPluginInterface {

  /**
   * Get the settings form for this plugin.
   *
   * @param \Drupal\contact_storage_remote\ConditionInterface $condition
   *   The condition.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form elements.
   */
  public function getSettingsForm(ConditionInterface $condition, FormStateInterface $form_state): array;

  /**
   * Process the submitted settings form values.
   *
   * @param \Drupal\contact_storage_remote\ConditionInterface $condition
   *   The condition.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function submitSettingsForm(ConditionInterface $condition, FormStateInterface $form_state): void;

  /**
   * Check if the condition matches.
   *
   * @param \Drupal\contact\MessageInterface $message
   *   The message.
   * @param \Drupal\contact_storage_remote\ConditionInterface $condition
   *   The condition.
   *
   * @return bool
   *   Matches or not.
   */
  public function matches(MessageInterface $message, ConditionInterface $condition): bool;

  /**
   * Get a summary of the condition.
   *
   * @param \Drupal\contact_storage_remote\ConditionInterface $condition
   *   The condition.
   *
   * @return string
   *   The summary.
   */
  public function getSummary(ConditionInterface $condition): string;

}
