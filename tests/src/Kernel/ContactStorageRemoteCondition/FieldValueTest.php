<?php

namespace Drupal\Tests\contact_storage_remote\Kernel\ContactStorageRemoteCondition;

use Drupal\contact\Entity\Message;
use Drupal\contact_storage_remote\Entity\Condition;
use Drupal\Tests\contact_storage_remote\Kernel\TestBase;

/**
 * Test the FieldValue condition plugin.
 */
class FieldValueTest extends TestBase {

  /**
   * TestMatches.
   */
  public function testMatches(): void {
    $message = Message::create([
      'contact_form' => $this->contactForm->id(),
      'mail' => 'test@domain.com',
    ]);
    $message->save();

    $condition = Condition::create([]);
    $condition->setSetting('field', 'mail');
    $condition->setSetting('property', 'value');
    $condition->setSetting('value', 'test@domain.com');

    /**
     * @var \Drupal\contact_storage_remote\ConditionPluginInterface $plugin
     */
    $plugin = $this->conditionPluginManager->createInstance('fieldvalue');

    $this->assertTrue($plugin->matches($message, $condition));

    $condition->setSetting('value', 'test1@domain.com');
    $this->assertFalse($plugin->matches($message, $condition));
  }

  /**
   * TestGetSummary.
   */
  public function testGetSummary(): void {
    $message = Message::create([
      'contact_form' => $this->contactForm->id(),
      'mail' => 'test@domain.com',
    ]);
    $message->save();

    $condition = Condition::create([]);
    $condition->setSetting('field', 'mail');
    $condition->setSetting('property', 'value');
    $condition->setSetting('value', 'test@domain.com');

    /**
     * @var \Drupal\contact_storage_remote\ConditionPluginInterface $plugin
     */
    $plugin = $this->conditionPluginManager->createInstance('fieldvalue');

    $this->assertEquals('mail.value = test@domain.com', $plugin->getSummary($condition));
  }

}
