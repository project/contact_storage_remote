<?php

namespace Drupal\Tests\contact_storage_remote\Kernel;

use Drupal\contact\Entity\Message;
use Drupal\contact_storage_remote\Entity\Condition;
use Drupal\contact_storage_remote_test\Plugin\ContactStorageRemoteStorage\TestStorage;

/**
 * Test the RemoteStoragePluginManager class.
 */
class RemoteStoragePluginManagerTest extends TestBase {

  /**
   * TestEnableDisablePlugin.
   */
  public function testEnableDisablePlugin(): void {
    $this->assertFalse($this->pluginManager->isPluginEnabled($this->contactForm, 'webhook'));
    $this->assertFalse(in_array('webhook', $this->pluginManager->getEnabledPlugins($this->contactForm)));
    $this->assertFalse($this->pluginManager->isPluginEnabled($this->contactForm2, 'webhook'));
    $this->assertFalse(in_array('webhook', $this->pluginManager->getEnabledPlugins($this->contactForm2)));

    $this->pluginManager->setPluginEnabled($this->contactForm, 'webhook', TRUE);

    $this->assertTrue($this->pluginManager->isPluginEnabled($this->contactForm, 'webhook'));
    $this->assertTrue(in_array('webhook', $this->pluginManager->getEnabledPlugins($this->contactForm)));
    $this->assertFalse($this->pluginManager->isPluginEnabled($this->contactForm2, 'webhook'));
    $this->assertFalse(in_array('webhook', $this->pluginManager->getEnabledPlugins($this->contactForm2)));

    $this->pluginManager->setPluginEnabled($this->contactForm, 'webhook', FALSE);

    $this->assertFalse($this->pluginManager->isPluginEnabled($this->contactForm, 'webhook'));
    $this->assertFalse(in_array('webhook', $this->pluginManager->getEnabledPlugins($this->contactForm)));
    $this->assertFalse($this->pluginManager->isPluginEnabled($this->contactForm2, 'webhook'));
    $this->assertFalse(in_array('webhook', $this->pluginManager->getEnabledPlugins($this->contactForm2)));
  }

  /**
   * TestStoreWithoutConditions.
   */
  public function testStoreWithoutConditions(): void {
    $this->assertCount(0, TestStorage::$stored);

    $this->pluginManager->setPluginEnabled($this->contactForm2, 'teststorage', TRUE);
    $this->pluginManager->store(Message::create([
      'contact_form' => $this->contactForm->id(),
    ]));
    $this->assertCount(0, TestStorage::$stored);

    $this->pluginManager->setPluginEnabled($this->contactForm, 'teststorage', TRUE);

    $this->pluginManager->store(Message::create([
      'contact_form' => $this->contactForm->id(),
    ]));
    $this->assertCount(1, TestStorage::$stored);

    // Test that plugin exceptions are catched.
    $this->pluginManager->setPluginEnabled($this->contactForm, 'teststorage', FALSE);
    $this->pluginManager->setPluginEnabled($this->contactForm, 'teststorageexception', TRUE);
    $this->pluginManager->store(Message::create([
      'contact_form' => $this->contactForm->id(),
    ]));
    $this->assertCount(1, TestStorage::$stored);
  }

  /**
   * TestStoreWithConditions.
   */
  public function testStoreWithConditions(): void {
    $this->pluginManager->setPluginEnabled($this->contactForm, 'teststorage', TRUE);

    $condition = Condition::create([
      'contactForm' => $this->contactForm->id(),
      'plugin' => 'fieldvalue',
    ]);
    $condition->setSetting('field', 'mail');
    $condition->setSetting('property', 'value');
    $condition->setSetting('value', 'address@domain.com');
    $condition->save();

    $message = Message::create([
      'contact_form' => $this->contactForm->id(),
      'mail' => 'test@test.com',
    ]);
    $message->save();

    $this->pluginManager->store($message);
    $this->assertCount(0, TestStorage::$stored);

    $message = Message::create([
      'contact_form' => $this->contactForm->id(),
      'mail' => 'address@domain.com',
    ]);
    $message->save();

    $this->pluginManager->store($message);
    $this->assertCount(1, TestStorage::$stored);
  }

  /**
   * TestFieldMapping.
   */
  public function testFieldMapping(): void {
    $field_mapping = $this->pluginManager->getFieldMapping($this->contactForm, 'nonexisting');
    $this->assertIsArray($field_mapping);
    $this->assertCount(0, $field_mapping);

    $field_mapping = $this->pluginManager->getFieldMapping($this->contactForm, 'webhook');
    $this->assertIsArray($field_mapping);
    $this->assertCount(0, $field_mapping);

    $this->assertNull($this->pluginManager->getFieldMappingForField($this->contactForm, 'webhook', 'email'));

    $this->pluginManager->setFieldMapping($this->contactForm, 'webhook', [
      [
        'field' => 'email',
        'maps_to' => 'mapped_email',
      ],
      [
        'field' => 'other',
        'maps_to' => 'other',
      ],
    ]);

    $field_mapping = $this->pluginManager->getFieldMapping($this->contactForm, 'webhook');
    $this->assertIsArray($field_mapping);
    $this->assertCount(2, $field_mapping);

    $this->assertEquals('mapped_email', $this->pluginManager->getFieldMappingForField($this->contactForm, 'webhook', 'email'));
    $this->assertNull($this->pluginManager->getFieldMappingForField($this->contactForm, 'nonexisting', 'email'));
  }

}
