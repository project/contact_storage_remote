<?php

namespace Drupal\Tests\contact_storage_remote\Kernel;

use Drupal\contact\Entity\Message;
use Drupal\Core\Form\FormState;

/**
 * Test the RemoteStoragePluginBaseTestWithFieldMappingOptions class.
 */
class RemoteStoragePluginBaseWithFieldMappingOptionsTest extends TestBase {

  /**
   * Test plugin.
   *
   * @var \Drupal\contact_storage_remote_test\Plugin\ContactStorageRemoteStorage\RemoteStoragePluginBaseTestPluginWithFieldMappingOptions
   */
  protected $plugin;

  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->plugin = $this->pluginManager->createInstance('remotestoragepluginbasetestwithfmo');
    $this->entityFieldManager = $this->container->get('entity_field.manager');
  }

  /**
   * TestGetFieldMappingOptions.
   */
  public function testGetFieldMappingOptions(): void {
    $message = Message::create([
      'contact_form' => $this->contactForm->id(),
    ]);
    $this->assertEquals(['mail' => 'mail'], $this->plugin->publicGetFieldMappingOptions($this->contactForm, $message->getFieldDefinition('mail'), new FormState()));
  }

  /**
   * TestBuildFieldMappingFormElement.
   */
  public function testBuildFieldMappingFormElement(): void {
    $fields = $this->entityFieldManager->getFieldDefinitions('contact_message', $this->contactForm->id());

    // Set a custom field mapping for name, and a predefined option for mail,
    // and unmap the rest.
    $field_mappings = [];
    foreach ($fields as $field) {
      $field_mappings[$field->getName()] = '';
    }
    $field_mappings['name.value'] = 'mapped_to_name';
    $field_mappings['mail.value'] = 'mail';
    $manager_field_mapping = [];
    foreach ($field_mappings as $field => $maps_to) {
      $manager_field_mapping[] = [
        'field' => $field,
        'maps_to' => $maps_to,
      ];
    }
    $this->pluginManager->setFieldMapping($this->contactForm, $this->plugin->getPluginId(), $manager_field_mapping);
    $element = $this->plugin->buildFieldMappingFormElement($this->contactForm, new FormState());

    $this->assertArrayHasKey('name.value', $element['field_mapping']);
    $this->assertEquals('custom', $element['field_mapping']['name.value']['maps_to_select']['#default_value']);
    $this->assertEquals($field_mappings['name.value'], $element['field_mapping']['name.value']['maps_to_custom']['#default_value']);

    $this->assertArrayHasKey('mail.value', $element['field_mapping']);
    $this->assertEquals('mail', $element['field_mapping']['mail.value']['maps_to_select']['#default_value']);
    $this->assertEquals('', $element['field_mapping']['mail.value']['maps_to_custom']['#default_value']);
  }

  /**
   * TestSubmitFieldMappingFormElement.
   */
  public function testSubmitFieldMappingFormElement(): void {
    $form_state = new FormState();
    $form_state->setValues([
      'field_mapping' => [
        'name.value' => [
          'maps_to_select' => 'custom',
          'maps_to_custom' => 'mapped_to_name',
        ],
        'mail.value' => [
          'maps_to_select' => 'mail',
          'maps_to_custom' => '',
        ],
      ],
    ]);

    $this->plugin->submitFieldMappingFormElement($this->contactForm, $form_state);

    $field_mappings = $this->pluginManager->getFieldMapping($this->contactForm, $this->plugin->getPluginId());

    $this->assertCount(2, $field_mappings);
    $required_mappings = [
      'name.value' => 'mapped_to_name',
      'mail.value' => 'mail',
    ];
    foreach ($field_mappings as $field_mapping) {
      if (isset($required_mappings[$field_mapping['field']]) && $required_mappings[$field_mapping['field']] === $field_mapping['maps_to']) {
        unset($required_mappings[$field_mapping['field']]);
      }
    }
    $this->assertCount(0, $required_mappings, 'The following fields were not found in the mapping: ' . json_encode($required_mappings));
  }

}
