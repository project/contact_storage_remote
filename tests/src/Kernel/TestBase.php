<?php

namespace Drupal\Tests\contact_storage_remote\Kernel;

use Drupal\contact\Entity\ContactForm;
use Drupal\KernelTests\KernelTestBase;

/**
 * Kernel base test class.
 */
abstract class TestBase extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'system',
    'contact',
    'contact_storage_remote',
    'contact_storage_remote_test',
  ];

  /**
   * Remote storage plugin manager.
   *
   * @var \Drupal\contact_storage_remote\RemoteStoragePluginManager
   */
  protected $pluginManager;

  /**
   * The condition plugin manager.
   *
   * @var \Drupal\contact_storage_remote\ConditionPluginManager
   */
  protected $conditionPluginManager;

  /**
   * Contact form.
   *
   * @var \Drupal\contact\ContactFormInterface
   */
  protected $contactForm;

  /**
   * Contact form2.
   *
   * @var \Drupal\contact\ContactFormInterface
   */
  protected $contactForm2;

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->installConfig(self::$modules);
    $this->installSchema('system', ['key_value', 'sequences']);
    $this->installEntitySchema('contact_form');
    $this->installEntitySchema('contact_storage_remote_condition');

    $this->pluginManager = $this->container->get('plugin.manager.contact_storage_remote.remote_storage');
    $this->conditionPluginManager = $this->container->get('plugin.manager.contact_storage_remote.condition');

    $this->contactForm = ContactForm::create([
      'id' => 'test',
      'label' => 'Test',
    ]);
    $this->contactForm->save();

    $this->contactForm2 = ContactForm::create([
      'id' => 'test2',
      'label' => 'Test2',
    ]);
    $this->contactForm2->save();
  }

}
