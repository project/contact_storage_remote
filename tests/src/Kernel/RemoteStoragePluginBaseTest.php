<?php

namespace Drupal\Tests\contact_storage_remote\Kernel;

use Drupal\contact\Entity\Message;
use Drupal\Core\Form\FormState;

/**
 * Test the RemoteStoragePluginBase class.
 */
class RemoteStoragePluginBaseTest extends TestBase {

  /**
   * Test plugin.
   *
   * @var \Drupal\contact_storage_remote_test\Plugin\ContactStorageRemoteStorage\RemoteStoragePluginBaseTestPlugin
   */
  protected $plugin;

  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->plugin = $this->pluginManager->createInstance('remotestoragepluginbasetest');
    $this->entityFieldManager = $this->container->get('entity_field.manager');
  }

  /**
   * TestIsEnabled.
   */
  public function testIsEnabled(): void {
    $this->assertFalse($this->plugin->publicIsEnabled($this->contactForm));
    $this->pluginManager->setPluginEnabled($this->contactForm, $this->plugin->getPluginId(), TRUE);
    $this->assertTrue($this->plugin->publicIsEnabled($this->contactForm));
    $this->pluginManager->setPluginEnabled($this->contactForm, $this->plugin->getPluginId(), FALSE);
    $this->assertFalse($this->plugin->publicIsEnabled($this->contactForm));
  }

  /**
   * TestGetFieldsToStore.
   */
  public function testGetFieldsToStore(): void {
    $message = Message::create([
      'contact_form' => $this->contactForm->id(),
      'name' => 'Ruud',
      'mail' => 'ruud@groowup.nl',
    ]);
    $message->save();

    // No mapped fields.
    $fields_to_store = $this->plugin->getFieldsToStore($message);
    $this->assertIsArray($fields_to_store);
    $this->assertCount(0, $fields_to_store);

    $this->pluginManager->setFieldMapping($this->contactForm, $this->plugin->getPluginId(), [
      [
        'field' => 'name.value',
        'maps_to' => 'mapped_to_name',
      ],
      [
        'field' => 'nonexisting',
        'maps_to' => 'something',
      ],
      [
        'field' => 'mail.value',
        'maps_to' => '',
      ],
    ]);

    $message = Message::create([
      'contact_form' => $this->contactForm->id(),
      'name' => 'Ruud',
      'mail' => 'ruud@groowup.nl',
    ]);
    $message->save();

    $fields_to_store = $this->plugin->getFieldsToStore($message);
    $this->assertIsArray($fields_to_store);
    $this->assertCount(1, $fields_to_store);
    $this->assertArrayHasKey('mapped_to_name', $fields_to_store);
    $this->assertEquals($message->get('name')
      ->getString(), $fields_to_store['mapped_to_name']);
  }

  /**
   * TestGetFieldMappingOptions.
   */
  public function testGetFieldMappingOptions(): void {
    $message = Message::create([
      'contact_form' => $this->contactForm->id(),
    ]);
    $this->assertNull($this->plugin->publicGetFieldMappingOptions($this->contactForm, $message->getFieldDefinition('mail'), new FormState()));
  }

  /**
   * TestBuildFieldMappingFormElement.
   */
  public function testBuildFieldMappingFormElement(): void {
    $fields = $this->entityFieldManager->getFieldDefinitions('contact_message', $this->contactForm->id());

    // No mapped fields, so all fields should be mapped to their system names.
    $element = $this->plugin->buildFieldMappingFormElement($this->contactForm, new FormState());
    foreach ($fields as $field) {
      foreach (array_keys($field->getFieldStorageDefinition()
        ->getSchema()['columns']) as $column) {
        $this->assertArrayHasKey($field->getName() . '.' . $column, $element['field_mapping']);
        $this->assertEquals($field->getName() . '.' . $column, $element['field_mapping'][$field->getName() . '.' . $column]['maps_to_custom']['#default_value']);
      }
    }

    // Set a field mapping for only name, and unmap the rest.
    $field_mappings = [];
    foreach ($fields as $field) {
      foreach (array_keys($field->getFieldStorageDefinition()
        ->getSchema()['columns']) as $column) {
        $field_mappings[$field->getName() . '.' . $column] = '';
      }
    }
    $field_mappings['name.value'] = 'mapped_to_name';
    $field_mappings['nonexisting'] = 'something';
    $manager_field_mapping = [];
    foreach ($field_mappings as $field => $maps_to) {
      $manager_field_mapping[] = [
        'field' => $field,
        'maps_to' => $maps_to,
      ];
    }
    $this->pluginManager->setFieldMapping($this->contactForm, $this->plugin->getPluginId(), $manager_field_mapping);
    $element = $this->plugin->buildFieldMappingFormElement($this->contactForm, new FormState());
    foreach ($fields as $field) {
      foreach (array_keys($field->getFieldStorageDefinition()
        ->getSchema()['columns']) as $column) {
        $this->assertArrayHasKey($field->getName() . '.' . $column, $element['field_mapping']);
        $this->assertEquals($field_mappings[$field->getName() . '.' . $column], $element['field_mapping'][$field->getName() . '.' . $column]['maps_to_custom']['#default_value']);
      }
    }
  }

  /**
   * TestSubmitFieldMappingFormElement.
   */
  public function testSubmitFieldMappingFormElement(): void {
    $form_state = new FormState();
    $form_state->setValues([
      'field_mapping' => [
        'name.value' => [
          'maps_to_select' => 'custom',
          'maps_to_custom' => 'mapped_to_name',
        ],
        'mail.value' => [
          'maps_to_select' => 'custom',
          'maps_to_custom' => '',
        ],
      ],
    ]);

    $this->plugin->submitFieldMappingFormElement($this->contactForm, $form_state);

    $field_mappings = $this->pluginManager->getFieldMapping($this->contactForm, $this->plugin->getPluginId());

    $this->assertCount(2, $field_mappings);
    $required_mappings = [
      'name.value' => 'mapped_to_name',
      'mail.value' => '',
    ];
    foreach ($field_mappings as $field_mapping) {
      if (isset($required_mappings[$field_mapping['field']]) && $required_mappings[$field_mapping['field']] === $field_mapping['maps_to']) {
        unset($required_mappings[$field_mapping['field']]);
      }
    }
    $this->assertCount(0, $required_mappings, 'The following fields were not found in the mapping: ' . json_encode($required_mappings));
  }

  /**
   * TestPluginSupportsFieldMapping.
   */
  public function testPluginSupportsFieldMapping(): void {
    $fields = $this->entityFieldManager->getFieldDefinitions('contact_message', $this->contactForm->id());
    $field_mappings = [];
    foreach ($fields as $field) {
      $field_mappings[$field->getName()] = '';
    }
    $field_mappings['name.value'] = 'mapped_to_name';
    $field_mappings['nonexisting'] = 'something';
    $manager_field_mapping = [];
    foreach ($field_mappings as $field => $maps_to) {
      $manager_field_mapping[] = [
        'field' => $field,
        'maps_to' => $maps_to,
      ];
    }
    $this->pluginManager->setFieldMapping($this->contactForm, 'teststorage', $manager_field_mapping);
    $this->pluginManager->setFieldMapping($this->contactForm, 'teststorageexception', $manager_field_mapping);

    /**
     * @var \Drupal\contact_storage_remote\RemoteStoragePluginInterface $plugin_with_fieldmapping
     */
    $plugin_with_fieldmapping = $this->pluginManager->createInstance('teststorage');
    /**
     * @var \Drupal\contact_storage_remote\RemoteStoragePluginInterface $plugin_without_fieldmapping
     */
    $plugin_without_fieldmapping = $this->pluginManager->createInstance('teststorageexception');

    $this->assertNotEmpty($plugin_with_fieldmapping->buildFieldMappingFormElement($this->contactForm, new FormState()));
    $this->assertEmpty($plugin_without_fieldmapping->buildFieldMappingFormElement($this->contactForm, new FormState()));

    $message = Message::create([
      'contact_form' => $this->contactForm->id(),
      'name' => 'Name',
      'mail' => 'mail@mail.com',
    ]);

    $fields_to_store = $plugin_with_fieldmapping->getFieldsToStore($message);
    $this->assertCount(1, $fields_to_store);
    $this->assertEquals('Name', $fields_to_store['mapped_to_name']);

    $fields_to_store = $plugin_without_fieldmapping->getFieldsToStore($message);
    $this->assertTrue(count($fields_to_store) > 2);
    $this->assertEquals('Name', $fields_to_store['name.value']);
    $this->assertEquals('mail@mail.com', $fields_to_store['mail.value']);
  }

}
