<?php

namespace Drupal\contact_storage_remote_test\Plugin\ContactStorageRemoteStorage;

use Drupal\contact\MessageInterface;
use Drupal\contact_storage_remote\RemoteStoragePluginBase;

/**
 * This plugin stores the store() parameter in a static variable.
 *
 * @ContactStorageRemoteStorage(
 *   id = "teststorage",
 *   title = @Translation("Test storage"),
 *   description = @Translation("Test storage."),
 *   supports_field_mapping = TRUE,
 *   supports_custom_field_mapping = TRUE
 * )
 */
class TestStorage extends RemoteStoragePluginBase {

  /**
   * Stored messages.
   *
   * @var \Drupal\contact\MessageInterface[]
   */
  public static $stored = [];

  /**
   * {@inheritDoc}
   */
  public function store(MessageInterface $message): bool {
    self::$stored[] = $message;
    return TRUE;
  }

}
