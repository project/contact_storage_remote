<?php

namespace Drupal\contact_storage_remote_test\Plugin\ContactStorageRemoteStorage;

use Drupal\contact\MessageInterface;
use Drupal\contact_storage_remote\RemoteStoragePluginBase;

/**
 * This plugins throws an exception when store() is called.
 *
 * @ContactStorageRemoteStorage(
 *   id = "teststorageexception",
 *   title = @Translation("Test storage exception"),
 *   description = @Translation("Test storage exception."),
 *   supports_field_mapping = FALSE,
 *   supports_custom_field_mapping = TRUE
 * )
 */
class TestStorageException extends RemoteStoragePluginBase {

  /**
   * {@inheritDoc}
   */
  public function store(MessageInterface $message): bool {
    throw new \Exception(__CLASS__);
  }

}
