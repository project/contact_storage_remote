<?php

namespace Drupal\contact_storage_remote_test\Plugin\ContactStorageRemoteStorage;

use Drupal\contact\ContactFormInterface;
use Drupal\contact\MessageInterface;
use Drupal\contact_storage_remote\RemoteStoragePluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Test implementation for RemoteStoragePluginBase.
 *
 * @ContactStorageRemoteStorage(
 *   id = "remotestoragepluginbasetestwithfmo",
 *   title = @Translation("remotestoragepluginbasetestwithfmo"),
 *   description = @Translation("remotestoragepluginbasetestwithfmo"),
 *   supports_field_mapping = TRUE,
 *   supports_custom_field_mapping = TRUE
 * )
 */
class RemoteStoragePluginBaseTestPluginWithFieldMappingOptions extends RemoteStoragePluginBase {

  /**
   * {@inheritDoc}
   */
  public function store(MessageInterface $message): bool {
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  protected function getFieldMappingOptions(ContactFormInterface $contact_form, $field, FormStateInterface $form_state): ?array {
    return [
      'mail' => 'mail',
    ];
  }

  /**
   * Public access to getFieldMappingOptions.
   *
   * @param \Drupal\contact\ContactFormInterface $contact_form
   *   The contact form.
   * @param \Drupal\Core\Field\BaseFieldDefinition|\Drupal\Core\Field\Entity\BaseFieldOverride $field
   *   The field we're mapping.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array|null
   *   The options or NULL if there are no predefined options to select
   */
  public function publicGetFieldMappingOptions(ContactFormInterface $contact_form, $field, FormStateInterface $form_state): ?array {
    return $this->getFieldMappingOptions($contact_form, $field, $form_state);
  }

}
