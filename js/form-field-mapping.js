(function (Drupal) {

  function mapsToSelectOnChange(e) {
    var value = e.target.value,
      field_name = e.target.getAttribute('data-field-name'),
      maps_to_custom = document.querySelector('input[data-field-name="' + field_name + '"]');

    if (!maps_to_custom) {
      return;
    }

    maps_to_custom.style.visibility = value === 'custom' ? '' : 'hidden';
  }

  Drupal.behaviors.contact_storage_remote_form_field_mapping = {
    attach: function (context, settings) {
      var elements = context.querySelectorAll('.contact-storage-remote-form-field-mapping select.maps-to-select'),
        i = elements.length;

      while (i--) {
        elements[i].addEventListener('change', mapsToSelectOnChange);
        mapsToSelectOnChange({
          target: elements[i]
        });
      }
    }
  };

})(Drupal);
