PROJECT
-------------
https://www.drupal.org/project/contact_storage_remote

INSTALLATION
-------------
1. Download and extract the module to your sites/all/modules/contrib folder.
2. Enable the module on the Drupal Modules page (admin/modules) or using :

   $ drush en

OVERVIEW
-------------
Contact Storage Remote allows you to store Contact messages on a remote
location by providing an interface to develop custom plugins to, for example,
connect to specific webservices.

An example plugin is included in the contact_storage_remote_webhook module
which allows you to POST the Contact message fields to an URL.

When enabled, a 'Remote storage' tab is added to the Contact form edit page.
Here you can enable and configure individual plugins per contact form, and
set up field mapping, if the plugin supports it.
