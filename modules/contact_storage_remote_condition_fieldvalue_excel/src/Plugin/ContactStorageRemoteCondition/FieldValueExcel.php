<?php

namespace Drupal\contact_storage_remote_condition_fieldvalue_excel\Plugin\ContactStorageRemoteCondition;

use Drupal\contact\Entity\Message;
use Drupal\contact\MessageInterface;
use Drupal\contact_storage_remote\ConditionInterface;
use Drupal\contact_storage_remote\ConditionPluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\file\FileInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin checks for a field value condition.
 *
 * @ContactStorageRemoteCondition(
 *   id = "fieldvalue_excel",
 *   title = @Translation("Field value from Excel document")
 * )
 */
class FieldValueExcel extends ConditionPluginBase {

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * File storage.
   *
   * @var \Drupal\file\FileStorageInterface
   */
  protected $fileStorage;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   The logger channel factory.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, LoggerChannelFactoryInterface $loggerChannelFactory, FileSystemInterface $fileSystem) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entityTypeManager, $loggerChannelFactory);
    $this->fileSystem = $fileSystem;
    $this->fileStorage = $entityTypeManager->getStorage('file');
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('logger.factory'),
      $container->get('file_system')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getSettingsForm(ConditionInterface $condition, FormStateInterface $form_state): array {

    $message = Message::create([
      'contact_form' => $condition->getContactForm()->id(),
    ]);

    $user_input = $form_state->getUserInput();
    $fieldvalue_field = isset($user_input['fieldvalue_field']) && !empty($user_input['fieldvalue_field']) ? $user_input['fieldvalue_field'] : $condition->getSetting('field');

    $form['fieldvalue_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Field'),
      '#options' => [],
      '#required' => TRUE,
      '#default_value' => $fieldvalue_field,
      '#ajax' => [
        'callback' => __CLASS__ . '::ajaxSelectFieldValueFieldCallback',
        'wrapper' => 'fieldvalue-property',
      ],
    ];

    foreach ($message->getFields() as $field_name => $field) {
      $form['fieldvalue_field']['#options'][$field_name] = $field->getFieldDefinition()
        ->getLabel();
    }

    $fieldvalue_property_options = [];
    $property = $condition->getSetting('property');
    if (!empty($fieldvalue_field) && $message->hasField($fieldvalue_field)) {
      $schema = $message->get($fieldvalue_field)
        ->getFieldDefinition()
        ->getFieldStorageDefinition()
        ->getSchema();

      if (!empty($property) && !isset($schema['columns'][$property])) {
        $property = NULL;
      }

      foreach ($schema['columns'] as $column => $data) {
        if (empty($property)) {
          $property = $column;
        }
        $fieldvalue_property_options[$column] = $column;
      }
    }

    $form['fieldvalue_property'] = [
      '#type' => 'select',
      '#title' => $this->t('Property'),
      '#options' => $fieldvalue_property_options,
      '#required' => TRUE,
      '#default_value' => $property,
      '#prefix' => '<div id="fieldvalue-property">',
      '#suffix' => '</div>',
    ];

    $excel = $condition->getSetting('excel');

    $form['fieldvalue_excel'] = [
      '#type' => 'managed_file',
      '#upload_location' => 'private://contact-storage-remote-condition-plugin',
      '#title' => $this->t('Excel document'),
      '#required' => TRUE,
      '#upload_validators' => [
        'file_validate_extensions' => 'xls xlsx',
      ],
      '#default_value' => is_numeric($excel) ? [$excel] : NULL,
    ];

    $column = $condition->getSetting('column');
    if (empty($column)) {
      $column = 'A';
    }

    $form['fieldvalue_column'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Column to use in Excel document'),
      '#description' => $this->t('Enter the column name to use for values (A, B, C, etc)'),
      '#default_value' => $column,
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * Re-render the property field.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed
   *   The render array
   */
  public static function ajaxSelectFieldValueFieldCallback(array &$form, FormStateInterface $form_state) {
    return $form['condition_plugin']['plugin_settings_form']['fieldvalue_property'];
  }

  /**
   * {@inheritDoc}
   */
  public function submitSettingsForm(ConditionInterface $condition, FormStateInterface $form_state): void {
    $old_fieldvalue_excel = $condition->getSetting('excel');
    $new_fieldvalue_excel = $form_state->getValue('fieldvalue_excel')[0];

    if (!empty($old_fieldvalue_excel) && $old_fieldvalue_excel != $new_fieldvalue_excel) {

      $old_file = $this->fileStorage->load($old_fieldvalue_excel);
      if ($old_file instanceof FileInterface) {
        $old_file->delete();
      }
    }

    /**
     * @var \Drupal\file\FileInterface $file
     */
    $file = $this->fileStorage->load($new_fieldvalue_excel);
    $file->setPermanent();
    $file->save();

    $condition->setSetting('field', $form_state->getValue('fieldvalue_field'));
    $condition->setSetting('property', $form_state->getValue('fieldvalue_property'));
    $condition->setSetting('excel', $new_fieldvalue_excel);
    $condition->setSetting('column', $form_state->getValue('fieldvalue_column'));
  }

  /**
   * {@inheritDoc}
   */
  public function matches(MessageInterface $message, ConditionInterface $condition): bool {
    $field = $condition->getSetting('field');
    $property = $condition->getSetting('property');
    $column = $condition->getSetting('column');

    $field_value = $message->get($field)->getValue();

    $field_property_value = $field_value[0][$property] ?? NULL;

    if (empty($field_property_value)) {
      return FALSE;
    }

    $file = $this->fileStorage->load($condition->getSetting('excel'));

    if (!$file instanceof FileInterface) {
      $this->logger->warning('File ' . $condition->getSetting('excel') . ' is missing.');
      return FALSE;
    }
    if (!file_exists($file->getFileUri())) {
      $this->logger->warning('File ' . $file->getFileUri() . ' is missing.');
      return FALSE;
    }
    $spreadsheet = IOFactory::load($this->fileSystem->realpath($file->getFileUri()));

    $worksheet = $spreadsheet->getActiveSheet();

    foreach ($worksheet->getRowIterator() as $row_index => $row) {
      $cell_value = $worksheet->getCell($column . $row_index);

      if (strtolower(trim($cell_value)) == strtolower(trim($field_property_value))) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function getSummary(ConditionInterface $condition): string {
    $field = $condition->getSetting('field');
    $property = $condition->getSetting('property');
    $column = $condition->getSetting('column');

    $file = $this->fileStorage->load($condition->getSetting('excel'));

    $summary = $field . '.' . $property . ' should be in column ' . $column;

    if ($file instanceof FileInterface) {
      $summary .= ' of ' . basename($file->getFilename());
    }

    return $summary;
  }

}
