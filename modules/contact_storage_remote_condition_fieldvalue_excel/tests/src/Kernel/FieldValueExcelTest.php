<?php

namespace Drupal\Tests\contact_storage_remote_condition_fieldvalue_excel\Kernel\ContactStorageRemoteCondition;

use Drupal\contact\Entity\Message;
use Drupal\contact_storage_remote\Entity\Condition;
use Drupal\Tests\contact_storage_remote\Kernel\TestBase;

/**
 * Test the FieldValue excel condition plugin.
 */
class FieldValueExcelTest extends TestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file',
    'contact_storage_remote_condition_fieldvalue_excel',
  ];

  /**
   * Test file.
   *
   * @var \Drupal\file\FileInterface
   */
  protected $testXlsFile;

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->installSchema('file', ['file_usage']);
    $this->installEntitySchema('file');

    /**
     * @var \Drupal\file\FileRepositoryInterface $file_repository
     */
    $file_repository = $this->container->get('file.repository');

    $this->testXlsFile = $file_repository->writeData(file_get_contents(__DIR__ . '/test.xls'), 'public://test.xls');
    $this->testXlsFile->setFileUri(__DIR__ . '/test.xls');
  }

  /**
   * TestMatches.
   */
  public function testMatches(): void {
    $message = Message::create([
      'contact_form' => $this->contactForm->id(),
      'mail' => 'test@domain.com',
    ]);
    $message->save();

    $condition = Condition::create([]);
    $condition->setSetting('field', 'mail');
    $condition->setSetting('property', 'value');
    $condition->setSetting('excel', $this->testXlsFile->id());
    $condition->setSetting('column', 'A');

    /**
     * @var \Drupal\contact_storage_remote\ConditionPluginInterface $plugin
     */
    $plugin = $this->conditionPluginManager->createInstance('fieldvalue_excel');

    $this->assertTrue($plugin->matches($message, $condition));

    $condition->setSetting('column', 'B');
    $this->assertFalse($plugin->matches($message, $condition));

    $message = Message::create([
      'contact_form' => $this->contactForm->id(),
      'mail' => 'test1@domain.com',
    ]);
    $message->save();
    $condition->setSetting('column', 'A');
    $this->assertFalse($plugin->matches($message, $condition));
  }

  /**
   * TestGetSummary.
   */
  public function testGetSummary(): void {
    $message = Message::create([
      'contact_form' => $this->contactForm->id(),
      'mail' => 'test@domain.com',
    ]);
    $message->save();

    $condition = Condition::create([]);
    $condition->setSetting('field', 'mail');
    $condition->setSetting('property', 'value');
    $condition->setSetting('value', 'test@domain.com');
    $condition->setSetting('excel', $this->testXlsFile->id());
    $condition->setSetting('column', 'A');

    /**
     * @var \Drupal\contact_storage_remote\ConditionPluginInterface $plugin
     */
    $plugin = $this->conditionPluginManager->createInstance('fieldvalue_excel');

    $this->assertEquals('mail.value should be in column A of test.xls', $plugin->getSummary($condition));
  }

}
