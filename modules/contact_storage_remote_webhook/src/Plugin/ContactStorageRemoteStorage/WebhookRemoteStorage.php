<?php

namespace Drupal\contact_storage_remote_webhook\Plugin\ContactStorageRemoteStorage;

use Drupal\contact\ContactFormInterface;
use Drupal\contact\MessageInterface;
use Drupal\contact_storage_remote\RemoteStoragePluginBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin that POSTs all message fields to an URL.
 *
 * @ContactStorageRemoteStorage(
 *   id = "webhook",
 *   title = @Translation("Webhook"),
 *   description = @Translation("Send the submitted field values to an URL."),
 *   supports_field_mapping = TRUE,
 *   supports_custom_field_mapping = TRUE
 * )
 */
class WebhookRemoteStorage extends RemoteStoragePluginBase {

  /**
   * HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->httpClient = $container->get('http_client');

    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function getSettingsForm(ContactFormInterface $contact_form, FormStateInterface $form_state): array {
    return [
      $this->getId() . '_webhookurl' => [
        '#type' => 'textfield',
        '#title' => $this->t('Webhook URL'),
        '#default_value' => $contact_form->getThirdPartySetting('contact_storage_remote_webhook', 'webhook_url', ''),
        '#element_validate' => [
          [$this, 'validateUrl'],
        ],
        '#contact_form' => $contact_form,
      ],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function validateUrl(array $element, FormStateInterface $form_state): void {

    if (!empty($element['#value']) && filter_var($element['#value'], FILTER_VALIDATE_URL) === FALSE) {
      $form_state->setError($element, $this->t('Invalid URL given.'));
      return;
    }

    if ($this->isEnabled($element['#contact_form']) && empty($element['#value'])) {
      $form_state->setError($element, $this->t('Please enter an URL.'));
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitSettingsForm(ContactFormInterface $contact_form, FormStateInterface $form_state): void {
    $contact_form->setThirdPartySetting('contact_storage_remote_webhook', 'webhook_url', $form_state->getValue($this->getId() . '_webhookurl'));
    $contact_form->save();
  }

  /**
   * {@inheritDoc}
   */
  public function getFieldsToStore(MessageInterface $message): array {
    $fields = parent::getFieldsToStore($message);

    foreach ($fields as $field => $value) {
      if (is_array($value)) {
        // Multi-valued fields are not supported.
        $fields[$field] = implode(',', $value);
      }
    }

    return $fields;
  }

  /**
   * {@inheritDoc}
   */
  public function store(MessageInterface $message): bool {
    $contact_form = $message->getContactForm();
    $post_fields = $this->getFieldsToStore($message);

    $url = $contact_form->getThirdPartySetting('contact_storage_remote_webhook', 'webhook_url');

    $this->logger->debug('POSTing message entity ' . $message->id() . ' to ' . $url);

    $this->httpClient->post($url, [
      'form_params' => $post_fields,
    ]);

    return TRUE;
  }

}
